# Exploratrices

_Note : comme le thème du jeu s’y prête particulièrement, j’ai fait le choix d’utiliser le féminin générique dans ce document (i.e. « joueuse » au lieu de « joueur »)._

Exploratrices est un prototype de jeu textuel qui se joue dans un navigateur Web.
On y incarne une Exploratrice qui part à l’aventure dans un monde sauvage.

Exploratrices n’a pas pour objectif d’être un jeu complet.
Il s’agit uniquement de tester des mécaniques de gameplay.
Deux mécaniques sont testées actuellement :

- la dimension « littéraire » : est-ce que c’est facile de lire ? (texte assez gros, bon contraste) agréable ? (pas d’effet « pavé » de texte, suffisamment bien écrit) divertissant ? (on reste accroché, on a envie de connaître la suite)
- les commandes : est-ce que c’est intuitif de saisir des commandes ? (une personne qui n’a pas l’habitude s’y met naturellement) agréable ? (on ne rechigne pas à les saisir) facile ? (pas besoin de se creuser les méninges pour s’en souvenir)

## Interface

L’écran du jeu se découpe en deux zones : l’écran qui affiche le texte, et l’invite de commandes qui permet de saisir et envoyer des commandes. L’ensemble est nommé « terminal ».

Aucun historique n’est géré à ce stade, que ce soit celui de l’écran (pour afficher les messages précédents) ou celui de l’invite de commandes (pour retrouver les commandes déjà saisies).

## Mécaniques

Exploratrices se découvre et se parcourt à travers des descriptions textuelles.
Le monde est composé de plusieurs « cartes ».
Les cartes sont connectées entre elles afin de pouvoir aller de l’une à l’autre.

### Se déplacer

Dans Exploratrices, la joueuse se déplace de carte en carte en saisissant la commande `/aller`.
Cette commande prend un argument de direction.
Les quatre directions possibles sont : `nord`, `est`, `sud` et `ouest`.
Exemple de commande complète : `/aller nord`.

Une fois la commande envoyée, si la destination est valide, la description de la carte vers laquelle la joueuse s’est dirigée est affichée à l’écran.

La description d’une carte consiste en un court paragraphe (2 à 3 phrases) et une liste de très courtes descriptions des directions. Par exemple :

> Vous vous trouvez au centre d’un petit village. Celui-ci est constitué de quelques vieilles bâtisses en mauvais état. Personne ne prête attention à vous.
>
> - Au **nord**, la route se poursuit vers un vieux moulin.
> - Au sud, une porte lourdement fermée vous barre le chemin.

Toutes les directions ne sont pas toujours décrites, ni toujours accessibles.
Dans l’exemple précédent, seulement les directions « nord » et « sud » sont décrites.
De plus, visiblement la direction « sud » n’est pas accessible.
Enfin, bien que les directions « est » et « ouest » ne soient pas décrites, elles sont potentiellement accessibles.

Pour aider la joueuse, les directions décrites et accessibles sont mises en évidence (en gras et avec de la couleur).

Si la joueuse essaye d’accéder à une direction invalide ou inaccessible, un message d’erreur générique est affiché à l’écran (« Vous ne pouvez pas aller dans cette direction. »)
Si la direction a été omise, la joueuse est invitée à ressaisir la commande avec l’argument de direction (« Dans quelle direction voulez-vous aller ? »)

### Observer

Afin d’en savoir plus sur la carte où elle se trouve, la joueuse peut observer son environnement avec la commande `/observer`.
Cette commande ne prend aucun argument.

Cela permet d’apporter du contexte à la joueuse grâce à une description plus longue (2 à 5 paragraphes environ) de l’endroit où la joueuse se trouve. Par exemple :

> Autour de vous, les habitantes du village vaquent à leur occupation.
> Un groupe de trois femmes discutent près d’un panneau en bois sur lequel des annonces sont accrochées.
> Là-bas, une autre puise son eau au puits.
> L’œil méfiant, un chat vous observe depuis la terrasse d’une maison.
>
> Vous ne pouvez vous empêcher de remarquer les haillons que portent les habitantes.
> Le village se trouve loin de tout, et il ne doit pas être facile de s’approvisionner en vêtements par ici.
>
> Plus loin, vous repérez un énorme silo à grains adossé à un vieux moulin.
> Du moins doivent-elles manger à leur faim si le silo est plein.
> Peut-être pourront-elles vous approvisionner ?

En principe, toutes les cartes ont une description détaillée de la sorte.
Toutefois, dans le cas où ce ne serait pas le cas, un message générique peut s’afficher (« Rien de particulier n’attire votre attention. »)

### Obtenir de l’aide

Afin que la joueuse ne se sente pas perdue, un système d’aide est disponible avec la commande `/aide`.
Cette commande ne prend aucun argument.

Lorsque la commande est saisie, une aide générique s’affiche pour rappeler les commandes disponibles et leur syntaxe.

## Tutoriel

Au début du jeu, la joueuse est invitée explicitement à saisir les commandes de déplacement et d’observation.
Cela lui permet de découvrir les mécaniques et d’apprendre à saisir les commandes.

Ce tutoriel est scénarisé pour améliorer l’immersion dans le jeu.

## Inscription & gestion des données

Aucune information n’est demandée à la joueuse pour pouvoir jouer.
Lorsqu’elle commence à jouer, un « token » unique est généré et stocké dans un cookie de son navigateur.

Le jeu n’a pas vocation à rester en ligne plus de 2 mois.
Les données des joueuses seront donc supprimées après cette période.

## Technique

Exploratrices est un jeu qui se joue dans un navigateur Web.

Mon but est d’utiliser la stack technique la plus simple possible : PHP (sans framework), HTML, CSS, JavaScript ([Turbo](https://turbo.hotwired.dev/) pour fluidifier le chargement).

Les cartes sont stockées sous forme de fichiers Yaml. Exemple de fichier Yaml pour une carte :

```yaml
---
position: "0,1"
descriptions:
    crossed: >
        Vous vous trouvez au centre d’un petit village. Celui-ci est constitué de quelques vieilles bâtisses en mauvais état. Personne ne prête attention à vous.

    observed: >
        Autour de vous, les habitantes du village vaquent à leur occupation. Un groupe de trois femmes discutent près d’un panneau en bois sur lequel des annonces sont accrochées. Là-bas, une autre puise son eau au puits. L’œil méfiant, un chat vous observe depuis la terrasse d’une maison.

        […]

    directions:
        north: la route se poursuit vers un vieux moulin.
        east:
        south: une porte lourdement fermée vous barre le chemin.
        west:

disabled_directions: ["west"]
---
```

De la même manière, les données des joueuses peuvent être stockées dans des fichiers Yaml. Exemple :

```yaml
---
id: "<token>"
position: "0,0"
---
```

Ici, le `<token>` correspond à l’identifiant unique généré pour la joueuse et stocké dans un cookie afin de l’identifier.

## Processus d’écriture et ChatGPT

L’un des buts du projet est de me donner l’occasion d’écrire de la fiction.
Si j’ai commencé avec plaisir, je me suis vite heurté à un problème à trois faces :

1. la peur de la page blanche me bloque
2. je ne souhaite pas passer trop de temps sur le projet
3. je suis démotivé à l’idée d’écrire

Pour me sortir de cette situation, j’ai pris la décision de « co-écrire » l’histoire avec [ChatGPT](https://chat.openai.com/chat).
Cette « Intelligence Artificielle » est à la mode en ce moment et je me suis dit que ce serait l’occasion de l’essayer.

Ma conclusion est que ChatGPT a été d’une grande aide pour me donner des idées pour « combler » l’univers.
Il m’a aussi donné l’occasion de me poser des questions que je ne me serais pas posées autrement.
Sur le plan uniquement littéraire, ChatGPT est toutefois assez pauvre.
Si j’ai gardé quelques phrases descriptives proposées par ChatGPT, j’en ai écrit la grande majorité moi-même.

Bien que ce fut un exercire « marrant », cela ne doit pas éclipser les critiques faites à ChatGPT.
Quelle utilité et impact social ?
Quel impact sur les travailleurs et travailleuses qui ont entraîné l’IA ?
Quels biais ?
De manière générale : veux-t-on de ChatGPT ?

Se pose aussi la question de la licence.
Puis-je placer ce travail sous licence CC BY-SA en prenant tout le crédit ?
Je n’ai pas de réponse définitive, mais je fais le choix d’en parler et de poser la question.

## Limitations connues

Le manque d’historique risque probablement de jouer en défaveur de l’agréabilité du gameplay.

À part se déplacer et observer (donc lire), il existe peu de mécaniques de jeu pour l’instant.
Cela pourrait générer un sentiment de frustration pour les joueuses.
Plusieurs évolutions sont envisageables pour l’avenir :

- ajouter un système d’inventaire (pour stocker des objets, ou pour des collections)
- ajouter un mécanisme à base de conditions (pour débloquer des passages par exemple)
- avoir des interactions entre joueuses

Tout cela n’a pas été développé par manque de temps.
Un pseudo-mécanisme de conditions existe pour donner un sentiment de fin aux joueuses.
Il s’agit toutefois d’un hack un peu sale (mais totalement ok dans le contexte de ce prototype).

## Licences

Le code du moteur de jeu est placé [sous licence AGPL v3.0 ou plus](/LICENSE.txt).

Le texte du jeu est quant à lui placé [sous licence CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/).

## Démarrer le serveur de développement

Installez PHP et l’extension `php-yaml` et démarrez le serveur de développement ainsi :

```console
$ php -t ./public/ -S 127.0.0.1:8000 ./public/index.php
```
