<?php

spl_autoload_register(
    function ($class_name) {
        $app_namespace = 'App\\';

        if (strpos($class_name, $app_namespace) === 0) {
            $class_name = substr($class_name, strlen($app_namespace));
            $class_path = str_replace('\\', '/', $class_name) . '.php';
            include APP_PATH . '/src/' . $class_path;
        }
    }
);
