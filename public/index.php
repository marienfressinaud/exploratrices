<?php

define('APP_PATH', realpath(__DIR__ . '/..'));

include APP_PATH . '/autoload.php';

$environment = getenv('APP_ENV');
if ($environment === false) {
    define('APP_ENV', 'development');
} else {
    define('APP_ENV', $environment);
}

if (APP_ENV === 'development' && \App\Utils::isPathPublic($_SERVER['PHP_SELF'])) {
    return false;
}

$screen = [];
$input = strtolower($_POST['command'] ?? '');

$session_token = $_COOKIE['session'] ?? '';
$user = null;
if ($session_token) {
    $user = \App\User::load($session_token);
    define('CAN_FINISH', (
        isset($user['condition_repos']) &&
        isset($user['condition_reunion']) &&
        isset($user['condition_marche'])
    ));
} else {
    define('CAN_FINISH', false);
}

if ($user) {
    $map = \App\Map::load($user['position']);

    if ($input !== '') {
        $command = new \App\Command($input, $user);
        $screen[] = [
            'type' => 'command',
            'content' => "/ {$command->input()}",
        ];

        try {
            $result = $command->execute();
            $screen[] = [
                'type' => 'output-success',
                'content' => $result,
            ];

            if ($command->name() === 'aller') {
                $user = \App\User::load($user['id']);
                $map = \App\Map::load($user['position']);
            } elseif (
                $command->name() === 'observer' &&
                $map['position'] === '1,1'
            ) {
                $user['condition_marche'] = true;
                \App\User::save($user);
            } elseif (
                $command->name() === 'observer' &&
                $map['position'] === '1,2'
            ) {
                $user['condition_repos'] = true;
                \App\User::save($user);
            } elseif (
                $command->name() === 'observer' &&
                $map['position'] === '2,1'
            ) {
                $user['condition_reunion'] = true;
                \App\User::save($user);
            }
        } catch (\Exception $exception) {
            $screen[] = [
                'type' => 'output-error',
                'content' => $exception->getMessage(),
            ];
        }
    } else {
        $screen[] = [
            'type' => 'output-success',
            'content' => \App\Map::display($map),
        ];
    }
} elseif ($input === 'jouer') {
    $user = \App\User::init();
    \App\User::save($user);

    $expires = new \DateTimeImmutable('+2 months');
    setcookie('session', $user['id'], [
        'expires' => $expires->getTimestamp(),
        'httponly' => true,
        'samesite' => 'Lax',
    ]);

    $map = \App\Map::load($user['position']);
    $screen[] = [
        'type' => 'output-success',
        'content' => \App\Map::display($map),
    ];
} else {
    $screen[] = [
        'type' => 'output-success',
        'content' => <<<TEXT
            Bienvenue dans Exploratrices, un jeu textuel en ligne de commandes. Pas d’inquiétude, vous serez accompagnée !

            <strong>Vous y incarnerez une Exploratrice qui arrive dans le village de Sylvaluna.</strong>

            <i>Attention, Exploratrices n’est actuellement qu’un prototype de jeu. Vous serez simplement amenée à visiter le village, découvrir ses habitantes et à en apprendre plus sur le monde des Exploratrices. Mais peut-être pourriez-vous également donner un coup de main aux habitantes du village ?</i>

            Pour jouer, il vous suffit de saisir des commandes dans l’invite de commandes en bas de votre écran. Pour commencer, veuillez saisir la commande <span class="command">jouer</span> et cliquer sur le bouton « envoyer » (ou appuyer sur la touche « <kbd>entrée</kbd> » de votre clavier).
            TEXT,
    ];
}

?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Exploratrices</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="style.css">
        <script src="turbo.js" defer></script>
    </head>

    <body>
        <div class="term">
            <div class="term__screen">
                <?php foreach ($screen as $output): ?>
                    <div class="screen__row screen__row--<?= $output['type'] ?>"><?= $output['content'] ?></div>
                <?php endforeach; ?>
            </div>

            <form class="term__prompt" method="post">
                <label for="command">Invite de commandes</label>
                <div class="term__prompt-container">
                    <span for="command">/</span>
                    <input id="command" type="text" name="command" value="" autofocus autocomplete="off" autocapitalize="off">
                    <button type="submit">envoyer</button>
                </div>
            </form>
        </div>

        <?php if (APP_ENV === 'development' && isset($map)): ?>
            <div class="debug">
                Position : <?= $map['position'] ?>
            </div>
        <?php endif; ?>
    </body>
</html>
