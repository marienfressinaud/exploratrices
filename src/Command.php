<?php

namespace App;

class Command
{
    private string $input;

    private string $name;

    private array $args;

    private array $user;

    public function __construct(string $input, array $user)
    {
        $this->input = self::sanitize(trim($input));
        $args = explode(' ', $this->input);
        $name = array_shift($args);
        $this->name = $name;
        $this->args = $args;
        $this->user = $user;
    }

    public function input(): string
    {
        return $this->input;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function args(): array
    {
        return $this->args;
    }

    public function execute(): string
    {
        $commandToExec = 'exec' . ucfirst($this->name);
        if (is_callable([$this, $commandToExec])) {
            return $this->$commandToExec();
        } else {
            throw new \Exception('Que voulez-vous dire ?');
        }
    }

    public function execAide(): string
    {
        return <<<TEXT
            aide
              → Affiche ce message d’aide.

            aller [DIRECTION]
              → Permet de vous déplacer sur la carte.
              → Remplacez [DIRECTION] par : <strong>nord</strong>, <strong>est</strong>, <strong>sud</strong>, ou <strong>ouest</strong>.

            observer
              → Permet d’en savoir plus sur votre environnement.

            regarder
              → Affiche l’endroit où vous vous trouvez.
            TEXT;
    }

    public function execObserver(): string
    {
        $map = Map::load($this->user['position']);
        if (!empty($map['descriptions']['observed'])) {
            return $map['descriptions']['observed'];
        } else {
            return 'Rien de particulier n’attire votre attention.';
        }
    }

    public function execAller(): string
    {
        $directions_translations = [
            'nord' => 'north',
            'est' => 'east',
            'sud' => 'south',
            'ouest' => 'west',
        ];
        $direction = implode(' ', $this->args);

        if (!$direction) {
            throw new \Exception('Dans quelle direction voulez-vous aller ?');
        }

        if (isset($directions_translations[$direction])) {
            $direction = $directions_translations[$direction];
        }

        $map = Map::load($this->user['position']);
        $allowed_directions = Map::allowedDirections($map);

        if (!in_array($direction, $allowed_directions)) {
            throw new \Exception('Vous ne pouvez pas aller dans cette direction.');
        }

        $new_position = Map::directionToPosition($map, $direction);
        $this->user['position'] = $new_position;
        User::save($this->user);

        $new_map = Map::load($new_position);
        return Map::display($new_map);
    }

    public function execRegarder(): string
    {
        $map = Map::load($this->user['position']);
        return Map::display($map);
    }

    private static function sanitize (string $content): string
    {
        return htmlspecialchars($content, ENT_COMPAT, 'UTF-8');
    }
}
