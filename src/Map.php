<?php

namespace App;

class Map
{
    public const MAPS_PATH = APP_PATH . '/data/maps';

    public static function load(string $position)
    {
        if (CAN_FINISH && $position === '2,2') {
            $position = '2,2-end';
        }

        $map_data_path = self::MAPS_PATH . "/{$position}.yaml";
        return yaml_parse_file($map_data_path);
    }

    public static function exists(string $position): bool
    {
        $map_data_path = self::MAPS_PATH . "/{$position}.yaml";
        return file_exists($map_data_path);
    }

    public static function display(array $map): string
    {
        $descriptions = $map['descriptions'];
        $output = $descriptions['crossed'];
        $allowed_directions = self::allowedDirections($map);

        $directions = '';
        if ($descriptions['directions']['north']) {
            $class = in_array('north', $allowed_directions) ? 'direction--allowed' : '';
            $directions .= "<li class=\"direction {$class}\">Au <span class=\"label\">nord</span>, {$descriptions['directions']['north']}</li>";
        }

        if ($descriptions['directions']['east']) {
            $class = in_array('east', $allowed_directions) ? 'direction--allowed' : '';
            $directions .= "<li class=\"direction {$class}\">À l’<span class=\"label\">est</span>, {$descriptions['directions']['east']}</li>";
        }

        if ($descriptions['directions']['south']) {
            $class = in_array('south', $allowed_directions) ? 'direction--allowed' : '';
            $directions .= "<li class=\"direction {$class}\">Au <span class=\"label\">sud</span>, {$descriptions['directions']['south']}</li>";
        }

        if ($descriptions['directions']['west']) {
            $class = in_array('west', $allowed_directions) ? 'direction--allowed' : '';
            $directions .= "<li class=\"direction {$class}\">À l’<span class=\"label\">ouest</span>, {$descriptions['directions']['west']}</li>";
        }

        if ($directions) {
            $output .= "<ul class=\"directions\">{$directions}</ul>";
        }

        return $output;
    }

    public static function allowedDirections(array $map): array
    {
        $allowed_directions = array_diff(
            ['north', 'east', 'south', 'west'],
            $map['disabled_directions'],
        );

        $allowed_directions = array_filter(
            $allowed_directions,
            function ($direction) use ($map) {
                $position = self::directionToPosition($map, $direction);
                return self::exists($position);
            },
        );

        return $allowed_directions;
    }

    public static function directionToPosition(array $map, string $direction): string
    {
        $position = $map['position'];
        list($x, $y) = explode(',', $position, 2);

        $x = intval($x);
        $y = intval($y);

        if ($direction === 'north') {
            $y += 1;
        } elseif ($direction === 'east') {
            $x += 1;
        } elseif ($direction === 'south') {
            $y -= 1;
        } elseif ($direction === 'west') {
            $x -= 1;
        }

        return "{$x},{$y}";
    }
}
