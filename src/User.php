<?php

namespace App;

class User
{
    public const USERS_PATH = APP_PATH . '/data/users';

    public static function init(): array
    {
        return [
            'id' => Random::hex(42),
            'position' => '-1,-2',
        ];
    }

    public static function save(array $user): void
    {
        $user_id = $user['id'];
        $user_data_path = self::USERS_PATH . "/{$user_id}.yaml";
        yaml_emit_file($user_data_path, $user);
    }

    public static function load(string $user_id)
    {
        $user_data_path = self::USERS_PATH . "/{$user_id}.yaml";
        return @yaml_parse_file($user_data_path);
    }
}
