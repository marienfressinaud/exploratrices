<?php

namespace App;

class Utils
{
    public static function isPathPublic (string $filepath): bool {
        if ($filepath === '/index.php') {
            return false;
        }

        $public_path = APP_PATH . '/public';
        $filepath = realpath($public_path . $filepath);
        return file_exists($filepath);
    }
}
